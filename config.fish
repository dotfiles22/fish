# Aliases

set -g fish_term24bit 1

set fish_greeting

set -Ux FZF_DEFAULT_OPTS "\
--color=bg+:#ccd0da,bg:#eff1f5,spinner:#dc8a78,hl:#d20f39 \
--color=fg:#4c4f69,header:#d20f39,info:#8839ef,pointer:#dc8a78 \
--color=marker:#dc8a78,fg+:#4c4f69,prompt:#8839ef,hl+:#d20f39"

set -gx BAT_THEME Catppuccin-latte
set -gx STARSHIP_CONFIG ~/.config/fish/starship.toml
starship init fish | source
