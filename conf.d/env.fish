mkdir -p ~/.local/bin/go
set -gx GOBIN ~/.local/bin/go
fish_add_path ~/.local/bin/go

set -x EDITOR hx