

fish_add_path ~/.cargo/bin
fish_add_path ~/.lib/go/bin
fish_add_path ~/.local/bin
fish_add_path /usr/locla/bin
fish_add_path ~/.npm-global/bin
fish_add_path ~/.config/composer/vendor/bin

fish_add_path ~/.config/fish/bin

if test -d ~/.config/helix/lsp
    fish_add_path ~/.config/helix/formatter/bin/
    fish_add_path ~/.config/helix/formatter/composer/bin/
    fish_add_path ~/.config/helix/lsp/composer/bin/
    fish_add_path ~/.config/helix/lsp/node/node_modules/.bin/
end
