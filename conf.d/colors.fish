#export LS_COLORS=(cat $HOME/.dircolors)

export color_background='#1A1B26'
export color_background_light='#3b4261'
export color_normal='#A9B1D6'
export color_primary='#DB4B4B'
export color_primary_text=$color_background
export color_alert='#E74856'
export color_alert_text=$color_normal
export color_warning='#FF9E64'
export color_warning_text=$color_normal
export color_success='#9ECE6A'
export color_success_text=$color_normal
export color_info="#7AA2F7"
export color_info_text=$color_normal
