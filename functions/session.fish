
function session

  if [ $argv[1] ]; [ $argv[1] = "new" ]
    if [ $argv[2] ]
      set session_name $argv[2]
    else 
      set session_name (gum input --placeholder "Nom de la session")
    end
    tmux new -s $session_name
  else
    if tmux list-sessions &>/dev/null
      set -l sessions (tmux list-sessions -F "#{session_name}")
      set -l sessions $sessions cancel
      set -l chosen_session (gum choose $sessions)

      if test "$chosen_session" = "cancel"
        echo "Canceled"
        exit 0
      end
      tmux attach -t $chosen_session
    else 
      echo "Aucune session active"
    end
  end
end
