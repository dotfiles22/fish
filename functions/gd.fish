
function gd
    echo "Fichiers stagés"
    for file in (git diff --name-only --staged)
        git diff --staged $file | delta --paging never --file-decoration-style='none' --file-style='hidden' --hunk-header-style='hidden' --width=(tput cols) | less --prompt="Staged $file" -r --tilde -c
        echo "○ $file"
    end;

    echo ""    
    echo "Fichiers non stagés"
    for file in (git ls-files --modified) 
        git diff $file | delta --paging never --file-decoration-style='none' --file-style='hidden' --hunk-header-style='hidden' --width=(tput cols) | less --prompt="Not-Staged $file" -r --tilde -c
        echo "○ $file"
    end;
    
    echo ""
    echo "Fichiers non pris en compte (À ajouter à git ou à ajouter au gitignore)"
    for file in (git ls-files --others --exclude-standard)
        echo "○ $file"
    end;
end
