

function gc
    set -l TYPE (gum choose "fix" "feat" "docs" "refactor" "chore" "ci")
    set -l SCOPE (gum input --placeholder "scope")

    # Since the scope is optional, wrap it in parentheses if it has a value.
    test -n "$SCOPE" &&
  set -l SCOPE "($SCOPE)"

    # Pre-populate the input with the type(scope): so that the user may change it
    set -l SUMMARY (gum input --value "$TYPE$SCOPE: " --placeholder "Sommaire")
    set -l DESCRIPTION (gum write --placeholder "Details (CTRL+D si fini)")

    # Commit these changes
    echo $SUMMARY
    echo $DESCRIPTION
    gum confirm "On commit ?" && git commit -m "$SUMMARY" -m "$DESCRIPTION"
end
