function notify_error
    if [ $argv[1] ]
       echo -n "$argv[1]" > ~/.local/tmp/notify-latest
        echo "[Error] $argv[1]" >> ~/.local/tmp/notifications
        echo "----------" >> ~/.local/tmp/notifications
        tmux split-window -l 1 -b -d 'echo -n " "; set_color -b red; set_color -o white; echo -n " Error "; set_color normal; echo -n " "; cat ~/.local/tmp/notify-latest; sleep 4'
    end
end
