
function notify
    if [ $argv[1] ]
        echo -n $argv[1] > ~/.local/tmp/notify-latest
        echo $argv[1] >> ~/.local/tmp/notifications
        echo "----------" >> ~/.local/tmp/notifications
        tmux split-window -l 1 -b -d 'cat ~/.local/tmp/notify-latest; sleep 4'
    end
end
