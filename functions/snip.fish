function snip
    mkdir -p ~/.config/snip/snippets
    if [ $argv[1] ]; [ $argv[1] = 'new' ]
        e ~/.config/snip/snippets/$argv[2]
    else
        tmux display-popup -E -w 95% -h 95% "fish -c snip_select"
        cd ~/.config/snip/
        php run.php (skate get snip) 
        cd -
        exit 0
    end
end
