
function show
    export PAGER="less --prompt=\Markdown -r --tilde -c"
    glow $argv[1] --pager --width (tput cols)
    set -e PAGER
end
